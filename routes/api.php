<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\Home@doLogin');
Route::post('logout', 'Api\Home@doLogout');
Route::get('hotels', 'Api\Home@hotel_list');
Route::get('find', 'Api\Home@find_hotel');
Route::get('find_availability', 'Api\Home@find_availability');
Route::post('create', 'Api\Home@create_hotel');
Route::post('update', 'Api\Home@update_hotel');
Route::post('delete', 'Api\Home@delete_hotel');