import React, { Component } from "react";
import './AddHotel.css';
import axios from "axios";

class AddHotel extends Component {
  state = {
    name: "",
    city: "",
    price: "",
    response: ""
  };

  onChangeHandler = e => this.setState({ [e.target.name]: e.target.value });

  addHotel = async e => {
    e.preventDefault();
    try {
      const hotels = await axios.post(`/api/create`, {
        name: this.refs.name.value,
        city: this.refs.city.value,
        price: this.refs.price.value
      });

      if(typeof hotels.data.status !== 'undefined' && hotels.data.status.message === 'Data Error!')
        this.setState({response: hotels.data.data.name });
      else 
        this.setState({response: hotels.data[0].message });
    } catch (err) {
      this.setState({ response: err.message });
    }
  };

  render() {
    return (
      <div className="AddUser-Wrapper">
        <h1>Add Hotel</h1>
        <form onSubmit={this.addHotel}>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            placeholder="Enter Name"
            name="name"
            onChange={this.onChangeHandler}
            ref="name"
            className="Add-User-Input"
            required
            id="name"
          />
          <label htmlFor="city">City: </label>
          <input
            type="text"
            placeholder="Enter City"
            name="city"
            onChange={this.onChangeHandler}
            ref="city"
            className="Add-User-Input"
            required
            id="city"
          />
          <label htmlFor="price">Price: </label>
          <input
            type="text"
            placeholder="Enter Price"
            name="price"
            onChange={this.onChangeHandler}
            ref="price"
            className="Add-User-Input"
            required
            id="price"
          />
          <button type="submit" className="Add-User-Submit fa fa-plus"></button>
          <button type="reset" className="Add-User-Reset fa fa-eraser"></button>
        </form>
        <p>{this.state.response}</p>
      </div>
    );
  }
}

export default AddHotel;
