import React, { Component } from "react";

class Paginate extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
      event.preventDefault();
      
      let pageno = event.target.id;
      this.props.paginateHotels('page='+pageno);
  }

  render() {
    const items = [];
    if(this.props.lastpage > 1) {
        for(let i = 1; i <= this.props.lastpage; i++) {
          let classname = (i === this.props.current_page) ? "active" : '';
          items.push(<a href="javascript:void(0)" className={classname} id={i} key={i} onClick={this.handleClick} >{i}</a>);
        }
    }

    return (
      <div className="pagination">
        {items}
      </div>
    );
  }
}

export default Paginate;