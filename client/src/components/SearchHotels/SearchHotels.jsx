import React, { Component } from "react";
import "./SearchHotels.css";

class SearchHotels extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state      = {
        name        : '',
        city        : '',
        price       : '',
        date_from   : '',
        date_to     : ''
    };
  }

  handleChange(event) {
      const target = event.target;
      const value  = target.value;
      const name   = target.name;

      this.setState({
          [name] : value
      })
  }

  handleSubmit(event) {
      event.preventDefault();
      let searchStr = '?name='+this.state.name;

      if(this.state.city !== '')
        searchStr  += '&city='+this.state.city;

      if(this.state.price !== '')
        searchStr  += '&price='+this.state.price;

      if(this.state.date_from !== '')
        searchStr  += '&date_from='+this.state.date_from;

      if(this.state.date_to !== '')
        searchStr  += '&date_to='+this.state.date_to;

      this.props.searchHotels(searchStr);
  }

  render() {
    return (
      <form className="form-inline" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input type="text" name="name" className="Search-User-Input" placeholder="Search by Name" value={this.state.name} onChange={this.handleChange}/>
        </div>
        <div className="form-group">
          <input type="text" name="city" className="Search-User-Input" placeholder="Search by City" value={this.state.city} onChange={this.handleChange}/>
        </div>
        <div className="form-group">
          <input type="text" name="price" className="Search-User-Input" placeholder="Search by Price Range e.g 100:200" value={this.state.price} onChange={this.handleChange}/>
        </div>
        <div className="form-group">
          <input type="date" name="date_from" className="Search-User-Input" placeholder="Search by From date" value={this.state.date_from} onChange={this.handleChange}/>
        </div>
        <div className="form-group">
          <input type="date" name="date_to" className="Search-User-Input" placeholder="Search by To date" value={this.state.date_to} onChange={this.handleChange}/>
        </div>
        <button type="submit" className="submit-button">Submit</button>
      </form>
    );
  }
}

export default SearchHotels;
