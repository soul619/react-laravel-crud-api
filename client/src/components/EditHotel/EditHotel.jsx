import React, { Component } from "react";
import './EditHotel.css';
import axios from "axios";
import { withRouter } from 'react-router'

class EditHotel extends Component {
  state = {
    id: '',
    name: "",
    city: "",
    price: "",
    response: ""
  };

  onChangeHandler = e => this.setState({ [e.target.name]: e.target.value });

  async componentDidMount() {
    try {
      
      let search =  this.props.location.search,
        id = search.substring(1, search.length);
      const updateHotel = await axios(`/api/find?hotel_id=${id}`);
      if(updateHotel.data[0].hotelist !== null) {
        const { name, city, price } = updateHotel.data[0].hotelist;
        this.setState({ id, name, city, price  });
      } else 
        this.setState({ response: "Hotel not found!" });
    } catch (err) {
      this.setState({ response: "Hotel not found!" });
    }
  };

  updateHotelHandler = async (e) => {
    e.preventDefault();
    try {
      const hotels = await axios.post(`/api/update`, {
        user_id: 1,
        access_token: 'i9YdRWcpagUO3nGuJjAHhdUskQ2latuo2y2t3Iw4StA0Pep75UudA1txxYNx',
        hotel_id: this.state.id,
        name: this.refs.name.value,
        city: this.refs.city.value,
        price: this.refs.price.value
      });
      this.setState({response: hotels.data[0].message });
    } catch (err) {
      this.setState({ response: err.message });
    }
  };

  render() {
    if (this.state.response === "Hotel not found!")
      return <h1>Hotel not found!</h1>
    return (
      <div className="Edit-User-Wrapper">
        <h1>Edit Hotel</h1>
        <form onSubmit={this.updateHotelHandler}>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            placeholder="Name..."
            value={ this.state.name }
            name="name"
            onChange={this.onChangeHandler}
            ref="name"
            className="Edit-User-Input"
            id="name"
          />
          <label htmlFor="city">City: </label>
          <input
            type="text"
            placeholder="City..."
            value={ this.state.city }
            name="city"
            onChange={this.onChangeHandler}
            ref="city"
            className="Edit-User-Input"
            id="city"
          />
          <label htmlFor="price">Price: </label>
          <input
            type="number"
            placeholder="Price"
            value={ this.state.price }
            name="price"
            onChange={this.onChangeHandler}
            ref="price"
            className="Edit-User-Input"
            id="price"
          />
          <button type="submit" className="Edit-User-Submit fa fa-pencil"></button>
        </form>
        <p>{this.state.response}</p>
      </div>
    );
  }
}

export default withRouter(EditHotel);
