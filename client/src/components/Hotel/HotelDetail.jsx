import React, { Component } from "react";
import axios from "axios";
import { withRouter } from 'react-router'

class HotelDetail extends Component {
  state = {
    id: '',
    name: "",
    data: "",
    response: ""
  };

  async componentDidMount() {
    try {
      
      let search =  this.props.location.search,
        id = search.substring(1, search.length);
      const updateHotel = await axios(`/api/find_availability?hotel_id=${id}`);
      if(updateHotel.data[0].hotelist !== null && updateHotel.data[0].hotelist.length > 0) {
        this.setState({ data: updateHotel.data[0], name: updateHotel.data[0].hotelname });
      } else 
        this.setState({ response: "Hotel not found!" });
    } catch (err) {
      this.setState({ response: "Hotel not found!" });
    }
  };

  render() {
    let hotels = [];
    if (this.state.data) {
      hotels =
        this.state.data.hotelist &&
        this.state.data.hotelist.map(hotel => (
          <tr key={hotel['id']}><td>{hotel['dates_from']}</td><td>{hotel['dates_to']}</td></tr>
        ));
    }
    
    if (this.state.response === "Hotel not found!")
      return <h1>Hotel not found!</h1>
    return (
      <div className="Edit-User-Wrapper">
        <h1>{this.state.name} </h1> Availability Dates
        <table className="Table">
          <thead>
            <tr>
              <th>From</th>
              <th>To</th>
            </tr>
          </thead>
          <tbody>{(hotels.length) ? hotels : <tr><td>No details found!</td></tr> }</tbody>
        </table>
        <br /><br/>
      </div>
    );
  }
}

export default withRouter(HotelDetail);
