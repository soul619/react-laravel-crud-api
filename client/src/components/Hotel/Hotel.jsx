import React from 'react';
import './Hotel.css';
import { Link } from 'react-router-dom';

const Hotel = ({ id, name, city, price, removeHotel }) => {

  return(
    <tr>
      <td>{ name }</td>
      <td>{ city }</td>
      <td>{ price }</td>
      <td>
        <button onClick={ () => removeHotel(id) } className="Action-Button fa fa-trash"></button>
        <Link to={'/edit?' + id}>
         <button className="Action-Button fa fa-pencil"></button>
        </Link>
        <Link to={'/detail?' + id}>
         <button className="Action-Button fa fa-info"></button>
        </Link>
      </td>
    </tr>
  );
};

export default Hotel;