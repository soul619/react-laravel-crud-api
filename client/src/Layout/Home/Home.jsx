import React, { Component } from "react";
import "./Home.css";
import axios from "axios";
import { GridLoader } from 'react-spinners';
// Components
import Hotel from "../../components/Hotel/Hotel";
import SearchHotels from "../../components/SearchHotels/SearchHotels";
import Paginate from "../../components/Pagination/Paginate";

class Home extends Component {
  state = {
    data: null,
    allHotels: null,
    searchStr: '',
    searchPage: 1,
    error: ""
  };

  async componentDidMount() {
    try {
      const hotels = await axios("/api/hotels/");
      this.setState({ data: hotels.data[0] });
    } catch (err) {
      this.setState({ error: err.message });
    }
  }

  removeHotel = async id => {
    try {
      const userRemoved = await axios.post("/api/delete/", {
          hotel_id: id
        }
      );
      const hotels = await axios("/api/hotels/");
      this.setState({ data: hotels.data[0] });
      alert('Hotel deleted successfully.');
    } catch (err) {
      this.setState({ error: err.message });
    }
  };

  searchHotels = async name => {
    const hotels = await axios("/api/hotels"+name);
    this.setState({ data: hotels.data[0] });
    this.setState({ searchStr: name });
  };

  paginateHotels = async name => {
    let searchstr = this.state.searchStr;
    searchstr += (searchstr !== '') ? '&'+name : '?'+name;
    const hotels = await axios("/api/hotels"+searchstr);
    this.setState({ data: hotels.data[0] });
    this.setState({ searchPage: name });
  };

  render() {
    let hotels;
    let lastpage = 1;
    let current_page = 1;
    if (this.state.data) {
      hotels =
        this.state.data.hotelist.data &&
        this.state.data.hotelist.data.map(hotel => (
          <Hotel key={hotel.id} {...hotel} removeHotel={this.removeHotel} />
        ));
        lastpage = (this.state.data.hotelist !== null) ? this.state.data.hotelist.last_page : 1;
        current_page = (this.state.data.hotelist !== null) ? this.state.data.hotelist.current_page : 1;
    } else 
      return <div className="Spinner-Wrapper"> <GridLoader color={'#333'} /> </div>;

    if (this.state.error) return <h4>{this.state.error}</h4>;
    
    return (
      <div className="Table-Wrapper">
        <h3>Search Hotels</h3>
        <SearchHotels searchHotels={this.searchHotels} />
        <br/> 
        <Paginate lastpage={lastpage} current_page={current_page} paginateHotels={this.paginateHotels}/>
        <table className="Table">
          <thead>
            <tr>
              <th>Name</th>
              <th>City</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{(hotels.length > 0) ? hotels : <tr><td>No hotel found!</td></tr> }</tbody>
        </table>
        <br /><br/>
      </div>
    );
  }
}

export default Home;
