import React, { Fragment } from 'react';
import AddHotel from '../../components/AddHotel/AddHotel';

const Add = () => {
  return (
    <Fragment>
      <AddHotel />
    </Fragment>
  );
};

export default Add;