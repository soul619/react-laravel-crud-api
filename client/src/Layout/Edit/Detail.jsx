import React, { Fragment } from 'react';
import HotelDetail from '../../components/Hotel/HotelDetail';

const Detail = () => {
  return (
    <Fragment>
      <HotelDetail />
    </Fragment>
  );
};

export default Detail;