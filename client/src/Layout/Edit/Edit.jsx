import React, { Fragment } from 'react';
import EditHotel from '../../components/EditHotel/EditHotel';

const Edit = () => {
  return (
    <Fragment>
      <EditHotel />
    </Fragment>
  );
};

export default Edit;