**How to setup and run the project**

This project is the combination of Laravel as a backend and React as a front end technologies. 
This is a simple crud system.

Clone or download the project.

---

## Setup Laravel Project

Laravel project is present in the root directory

1. Composer install/update.
2. Rename .env-example file to .env.
3. Create table in mysql and modify env file.
4. php artisan migrate.
5. php artisan db:seed.
6. php artisan serve.

---

## Setup React Project

React project is present in the client directory.

1. npm install.
2. npm start.

---