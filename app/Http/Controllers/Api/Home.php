<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Property;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use App\Hotels;
use App\User;
use App\HotelAvailabilities;

class Home extends Controller
{
    public function __construct() {
    }

    public function hotel_list() {
        $result                     = [];
        $result['hotelist']         = [];
        $userdata                   = Input::get();
        if(isset($userdata) && !empty($userdata)) {
            try { 
                $query                  = DB::table('hotels');

                if(isset($userdata['name']))
                    $query = $query->where('name', 'like', '%'.$userdata['name'].'%');

                if(isset($userdata['city']))
                    $query = $query->where('city', 'like', '%'.$userdata['city'].'%');

                if(isset($userdata['price'])) {
                    $pricerange = explode(':', $userdata['price']);
                    if(isset($pricerange[1])) {
                        $query = $query->whereBetween('price', [$pricerange[0], $pricerange[1]]);
                    } else 
                        $query = $query->where('price', $pricerange[0]);
                }

                if(isset($userdata['sortby'])) { 
                    $sort  = (isset($userdata['sort'])) ? $userdata['sort'] : 'asc';
                    $query = $query->orderBy($userdata['sortby'], $sort);
                }
            } catch (\Illuminate\Database\QueryException $ex) { 
                $response['response'] = 18;
            }

            $result['hotelist'] = $query->paginate(10);
        } else 
            $result['hotelist']         = Hotels::paginate(10);

        return response()->json([$result], 200);
    }
    
    public function find_hotel() {
        $result                     = [];
        $result['hotelist']         = [];
        $userdata                   = Input::get();
        if(isset($userdata) && !empty($userdata) && isset($userdata['hotel_id'])) {
            try { 
                $result['hotelist'] = Hotels::find($userdata['hotel_id']);
            } catch (\Illuminate\Database\QueryException $ex) { 
                $response['response'] = 18;
            }
        } else 
            $result['hotelist']         = Hotels::paginate(10);

        return response()->json([$result], 200);
    }

    public function find_availability() {
        $result                     = [];
        $result['hotelist']         = [];
        $userdata                   = Input::get();
        if(isset($userdata) && !empty($userdata) && isset($userdata['hotel_id'])) {
            try { 
                $result['hotelname'] = Hotels::find($userdata['hotel_id'],'name');
                if(isset($result['hotelname']['name']) && $result['hotelname']['name']) { 
                    $result['hotelname'] = $result['hotelname']['name'];
                    $result['hotelist']  = Hotels::find($userdata['hotel_id'])->hotel_availabilities;
                }
                $response['response'] = 0;
            } catch (\Illuminate\Database\QueryException $ex) { 
                $response['response'] = 18;
            }
        }

        return response()->json([$result], 200);
    }

    public function create_hotel(Request $request) {
        $response = [];
        $response['response'] = $hotel_id = 0;
        $userdata = $request->all();

        if(isset($userdata) && !empty($userdata)) {
            /*
                * Validation
            */
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|unique:hotels',
                'city' => 'required|string',
                'price' => 'required',
            ]);

            if($validator->fails()){
                $errors = $validator->errors();
                return response()->json(['status' => ['code' => 2000, 'message' => 'Data Error!'], 'data' => $errors]);
            } else {
                try { 
                    /*
                    * Insert Hotel
                    */
                    $data = new Hotels;
                    $data->name = $userdata['name'];
                    $data->price = $userdata['price'];
                    $data->city = $userdata['city'];
                    $data->created_at = date("Y-m-d H:i:s");
                    $data->updated_at = date("Y-m-d H:i:s");

                    if ($data->save()) {
                        $response['response'] = 1;
                        $response['message'] = 'Hotel Created Successfully.';
                        $hotel_id = $data->id;
                    }
                } catch (\Illuminate\Database\QueryException $ex) { 
                    $response['response'] = 18;
                }
            }

            $error_response = $this->generate_error_code($response['response']);
            if(!empty($error_response)) {
                if($error_response['code'] == 200 && $hotel_id) {
                    $response['hotel']  = Hotels::find($hotel_id);
                } else {
                    if(isset($error_response['message']) && $error_response['message'] != '')
                        $response['message'] = $error_response['message'];
                    else
                    $response['message'] = "Unable to perform this operation.";
                }
                return response()->json([$response], $error_response['code']);
            }
        }
        $response['message'] = "Bad request.";
        return response()->json($response, 202);
    }

    public function update_hotel(Request $request) {
        $response = [];
        $response['response'] = $hotel_id = 0;
        $userdata = $request->all();

        if(isset($userdata) && !empty($userdata)) {
        
            $hotel_id  = (isset($userdata['hotel_id'])) ? $userdata['hotel_id']: 0;
            $hoteldata = Hotels::find($hotel_id);
            if($hotel_id && !empty($hoteldata)) {
                /*
                    * Validation
                */
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string',
                    'city' => 'required|string',
                    'price' => 'required',
                ]);

                if($validator->fails()) {
                    $errors = $validator->errors();
                    return response()->json(['status' => ['code' => 2000, 'message' => 'Data Error!'], 'data' => $errors]);
                } else {
                    try { 
                        /*
                        * Update Hotel
                        */
                        DB::table('hotels')->where('id', $hotel_id)->update([
                            'name' => $userdata['name'],
                            'city' => $userdata['city'],
                            'price' => $userdata['price'],
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                        $response['response'] = 1;
                        $response['message'] = 'Hotel Updated Successfully.';
                    } catch (\Illuminate\Database\QueryException $ex) { 
                        $response['response'] = 18;
                    }
                }    
            } else 
                $response['response'] = 17;

            $error_response = $this->generate_error_code($response['response']);
            if(!empty($error_response)) {
                if($error_response['code'] == 200 && $hotel_id) {
                    $response['hotel']  = Hotels::find($hotel_id);
                } else {
                    if(isset($error_response['message']) && $error_response['message'] != '')
                        $response['message'] = $error_response['message'];
                    else
                    $response['message'] = "Unable to perform this operation.";
                }
                return response()->json([$response], $error_response['code']);
            }
        }
        $response['message'] = "Bad request.";
        return response()->json($response, 202);
    }

    public function delete_hotel(Request $request) {
        $response = [];
        $response['response'] = $hotel_id = 0;
        $userdata = $request->all();

        if(isset($userdata) && !empty($userdata)) {
            $hotel_id  = (isset($userdata['hotel_id'])) ? $userdata['hotel_id']: 0;
            $hoteldata = Hotels::find($hotel_id);
            if($hotel_id && !empty($hoteldata)) {
                try { 
                    Hotels::find($hotel_id)->delete();
                    $response['response'] = 1;
                    $response['message'] = 'Hotel Deleted Successfully.';
                } catch (\Illuminate\Database\QueryException $ex) { 
                    $response['response'] = 18;
                }
            } else 
                $response['response'] = 17;
        
            $error_response = $this->generate_error_code($response['response']);
            if(!empty($error_response)) {
                if($error_response['code'] == 200 && $hotel_id) {
                    $response['hotel']  = Hotels::find($hotel_id);
                } else {
                    if(isset($error_response['message']) && $error_response['message'] != '')
                        $response['message'] = $error_response['message'];
                    else
                    $response['message'] = "Unable to perform this operation.";
                }
                return response()->json([$response], $error_response['code']);
            }
        }
        $response['message'] = "Bad request.";
        return response()->json($response, 202);
    }

    public function generate_error_code($response) {
        $result = array();
        if($response == '1' || $response == 1){
            $result['code']    = 200;
        } else if($response == '17' || $response == 17){
            $result['message'] = "Invalid Hotel Id.";
            $result['code']    = 218;
        } else if($response == '18' || $response == 18){
            $result['message'] = "Invalid Query.";
            $result['code']    = 219;
        } else {
            $result['message'] = "Invalid paramters.";
            $result['code']    = 204;
        }
        
        return $result;
    }
}
