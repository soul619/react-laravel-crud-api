<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelAvailabilities extends Model
{
    protected $fillable = ['hotel_id', 'dates_from', 'pridates_to'];
}
