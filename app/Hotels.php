<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotels extends Model
{
    protected $fillable = ['name', 'city', 'price'];

    /**
     * Get the hotel availabilities for the hotel.
     */
    public function hotel_availabilities()
    {
        return $this->hasMany('App\HotelAvailabilities', 'hotel_id');
    }
}
