<?php

use App\HotelAvailabilities;
use Illuminate\Database\Seeder;

class HotelAvailabilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        HotelAvailabilities::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few HotelAvailabilities in our database:
        for ($i = 1; $i < 50; $i++) {
            $rand = rand(2,4);
            $cur  = date('Y-m-d');

            for($j = 1; $j <= $rand; $j++) {
                $prev = $cur;
                $cur  = date('Y-m-d', strtotime("+ ".$rand."day", strtotime($cur))); 
                HotelAvailabilities::create([
                    'hotel_id' => $i,
                    'dates_from' => $prev,
                    'dates_to' => $cur,
                ]);
            }
        }
    }
}
