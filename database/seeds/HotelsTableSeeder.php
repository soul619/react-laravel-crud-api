<?php
use App\Hotels;
use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Hotels::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few Hotelss in our database:
        for ($i = 0; $i < 50; $i++) {
            Hotels::create([
                'name' => $faker->streetName.' Hotel',
                'city' => $faker->city,
                'price' => $faker->randomFloat(2, 100, 700),
            ]);
        }
    }
}
